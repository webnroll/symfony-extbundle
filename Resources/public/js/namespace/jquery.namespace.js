(function ($) {
    $._paramsQueue = {};
    $.ns = function (namespaceName, obj) {
        var names = namespaceName.split(".");
        var previousNs = $;
        for (var i = 0; i < names.length; i++) {
            var sp = names[i];
            if ((sp.length == 0) || (sp == '$')) continue;
            if (!previousNs[sp]) {
                previousNs[sp] = {};
                if (i == names.length - 1) {
                    $._setParams(namespaceName, obj);
                    previousNs[sp] = obj;

                }
            }
            previousNs = previousNs[sp];
        }
        if ($.exists(previousNs)) {
            if ($.exists(obj._init) && ($.isFunction(obj._init))) {
                $(document).ready(function () {
                    obj._init.apply(obj);
                });
            }
            if ($.exists(obj._events)) {
                $(document).ready(function () {
                    for (var evth in obj._events) {
                        var splat = evth.split("_");
                        if (splat[0] == evth) continue;
                        var id = splat[0];
                        var evts = "";
                        for (var i = 1; i < splat.length; i++) {
                            evts = evts + splat[i].toLowerCase() + " ";
                        }
                        evts = evts.trim();

                        var element = $("#" + id);
                        if (element.length == 0) continue;
                        element.bind(evts, obj._events[evth]);
                    }
                });
            }
        }

    };
    $._setParams = function (namespaceName, obj) {
        if ($.exists($._paramsQueue[namespaceName])) {
            obj._parameters = $._paramsQueue[namespaceName];
        }
    };

    $.notExists = function (obj) {
        if (!obj) return true;
        return false;
    };

    $.nullOrEmpty = function (obj) {
        if (obj == null) return true;
        if (obj == undefined) return true;
        if (obj == "") return true;
        if (($.exists(obj.length)) && (obj.length == 0)) return true;
        return false;
    };

    $.functionName = function (func) {
        var m = func.toString().match(/^function ([^(]+)/);
        if (m)
            return m[1];
        return null;
    };

    $.exists = function (obj) {
        return !$.notExists(obj);
    };

    $.nsParameters = function (namespaceName, parametersObj) {
        var ns = $.getNs(namespaceName);
        if (!ns) {
            $._paramsQueue[namespaceName] = parametersObj;
        } else {
            $.applyObject(parametersObj, ns);
            ns._parameters = parametersObj;
        }
        return ns;
    };

    $.applyObject = function (fromModel, toModel) {
        for (var p in fromModel) {
            var g = fromModel[p];
            var f = toModel[p];
            if (f != null) {
                if (typeof(f) == "object") continue;

                var fn = $.functionName(f);
                if (fn) {
                    if (fn == "c") {
                        f(g());
                    }
                } else {
                    toModel[p] = g;
                }
            }
        }
    };

    $.getNs = function (namespaceName) {
        var names = namespaceName.split(".");
        var previousNs = $;
        for (var i = 0; i < names.length; i++) {
            var sp = names[i];
            if ((sp.length == 0) || (sp == '$')) continue;
            previousNs = previousNs[sp];
            if (!previousNs) return null;

        }
        return previousNs;
    };


    $._wrapFunctionCall = function (strFunctionCall) {
        if (strFunctionCall.startsWith('return')) {
            strFunctionCall = strFunctionCall.substring('return'.length, strFunctionCall.length).trim();
        }
        var indexOfargs = strFunctionCall.indexOf('(');
        var namepaceAndFunction = strFunctionCall.substring(0, indexOfargs);

        var indexOfArgsEnd = strFunctionCall.lastIndexOf(')');
        var arguments = strFunctionCall.substring(indexOfargs + 1, indexOfArgsEnd);

        var functionNameSeparatorIdx = namepaceAndFunction.lastIndexOf('.');
        var functionName = namepaceAndFunction.substring(functionNameSeparatorIdx + 1, namepaceAndFunction.length);
        var namespaceName = namepaceAndFunction.substring(0, functionNameSeparatorIdx);
        var functionCallWrapped = 'return ' + namepaceAndFunction + '.apply(' + namespaceName + ',[' + arguments + ']);';
        return functionCallWrapped;
    };

    $.hookAllOnSomethingMethods = function () {
        var events = ['onclick', 'onload'];
        var funs = ['$.Cordage', 'return'];
        var patterns = [];
        for (var i = 0; i < events.length; i++) {
            var evt = events[i];
            for (var j = 0; j < funs.length; j++) {
                var fun = funs[j];
                var pattern = '[' + evt + '^="' + fun + '"]';
                patterns = patterns.concat({ Pattern: pattern, Event: evt });
            }
        }
        for (i = 0; i < patterns.length; i++) {
            var pttrn = patterns[i];
            $(pttrn.Pattern).each(function () {
                var attr = $(this).attr(pttrn.Event);
                var wrapped = $._wrapFunctionCall(attr);
                $(this).attr(pttrn.Event, wrapped);
            });
        }
    };
    $.serializeForPost = function (object, paramName, amp) {
        var s = "";
        if (!paramName) paramName = "";
        if ($.isArray(object)) {
            for (var i = 0; i < object.length; i++) {
                if (i > 0) {
                    amp = true;
                }
                s += $.serializeForPost(object[i], paramName + "[" + i + "]", amp);

            }
            if (amp) {
                s = "&" + s;
            }
            return s;
        }
        if ($.isPlainObject(object)) {
            var namp = false;
            for (var v in object) {
                var vi = object[v];
                var pname = (paramName.length == 0 ? v : paramName + "." + v);
                s += $.serializeForPost(vi, pname, amp);
                if (!namp) {
                    namp = true;
                    amp = true;
                }
            }
        } else {
            if ($.isFunction(object)) return "";
            var objetstr = object != null ? object.toString() : "null";
            s = (amp ? "&" : "") + paramName + "=" + objetstr;
        }
        return s;
    };
    $.populateDropdown = function(id, rows, textName, valueName) {
        if (id[0] != '#') {
            id = '#' + id;
        }
        $(id).empty();
        $(rows).each(function(index) {
            var row = this;
            $(id).append('<option value="' + row[valueName] + '">' + row[textName] + '</option>');
        });
    };
} (jQuery));