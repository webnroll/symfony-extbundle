<?php

namespace Webnroll\ExtBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Assetic\Asset\FileAsset;
use Assetic\Asset\HttpAsset;

use Symfony\Component\HttpFoundation\Response;

class ExtController extends Controller
{
    /**
     * @Template
     * @Route("/common")
     *
     * @return array
     */
    public function commonAction($renderType = 'both')
    {
        return array(
            'render_type' => $renderType,
        );
    }

    /**
     * @Template
     * @Route("/jqueryui")
     *
     * @return array
     */
    public function jqueryuiAction($dynamic = true, $theme = 'redmond', $renderType = 'both')
    {
        if ($dynamic) {
            $this->container->get('ext.dynamic.assets')->addJs(
                $this->container->get('templating.helper.assets')
                    ->getUrl('bundles/webnrollext/js/jqueryui/jquery-ui-latest.min.js')
            );

            $this->container->get('ext.dynamic.assets')->addCss(
                $this->container->get('templating.helper.assets')
                    ->getUrl("bundles/webnrollext/css/jqueryui/{$theme}/jquery-ui.min.css")
            );
            $this->container->get('ext.dynamic.assets')->addCss(
                $this->container->get('templating.helper.assets')
                    ->getUrl("bundles/webnrollext/css/jqueryui/{$theme}/jquery.ui.theme.css")
            );
        }

        return array(
            'dynamic' => $dynamic,
            'theme' => $theme,
            'render_type' => $renderType,
        );
    }

    /**
     * @Template
     * @Route("/jqgrid")
     *
     * @return array
     */
    public function jqgridAction($dynamic = true, $locale = null, $renderType = 'both', $bootstrap = false)
    {
        if ($locale == null) {
            $request = $this->container->get('request');
            $locale = $request->getLocale();
            if (!$locale) {
                $locale = 'en';
            }
        }

        if ($dynamic) {
            $this->container->get('ext.dynamic.assets')->addJs(
                $this->container->get('templating.helper.assets')
                    ->getUrl('bundles/webnrollext/js/jqgrid/jquery.jqGrid.min.js')
            );

            //  localization
            $this->container->get('ext.dynamic.assets')->addJs(
                $this->container->get('templating.helper.assets')
                    ->getUrl("bundles/webnrollext/js/jqgrid/i18n/grid.locale-{$locale}.js")
            );

            $this->container->get('ext.dynamic.assets')->addCss(
                $this->container->get('templating.helper.assets')
                    ->getUrl("bundles/webnrollext/css/jqgrid/ui.jqgrid.css")
            );
        }

        return array(
            'dynamic' => $dynamic,
            'locale' => $locale,
            'render_type' => $renderType,
            'bootstrap' => $bootstrap,
        );
    }

    /**
     * @Template
     * @Route("/bootstrap")
     *
     * @return array
     */
    public function bootstrapAction($dynamic = true, $renderType = 'both')
    {
        if ($dynamic) {
            $this->container->get('ext.dynamic.assets')->addJs(
                $this->container->get('templating.helper.assets')
                    ->getUrl('bundles/webnrollext/js/bootstrap/bootstrap.min.js')
            );

            $this->container->get('ext.dynamic.assets')->addCss(
                $this->container->get('templating.helper.assets')
                    ->getUrl("bundles/webnrollext/css/bootstrap/bootstrap.min.css")
            );

            $this->container->get('ext.dynamic.assets')->addCss(
                $this->container->get('templating.helper.assets')
                    ->getUrl("bundles/webnrollext/css/bootstrap/bootstrap-theme.min.css")
            );
        }

        return array(
            'dynamic' => $dynamic,
            'render_type' => $renderType,
        );
    }

    /**
     * @Template
     * @Route("/namespace")
     *
     * @return array
     */
    public function namespaceAction($dynamic = true, $renderType = 'both')
    {
        if ($dynamic) {
            $this->container->get('ext.dynamic.assets')->addJs(
                $this->container->get('templating.helper.assets')
                    ->getUrl('bundles/webnrollext/js/namespace/jquery.namespace.js')
            );
        }

        return array(
            'dynamic' => $dynamic,
            'render_type' => $renderType,
        );
    }

    /**
     * @Template
     * @Route("/jqform")
     *
     * @return array
     */
    public function jqformAction($dynamic = true, $renderType = 'both')
    {
        if ($dynamic) {
            $this->container->get('ext.dynamic.assets')->addJs(
                $this->container->get('templating.helper.assets')
                    ->getUrl('bundles/webnrollext/js/jqform/jquery.form.min.js')
            );
        }

        return array(
            'dynamic' => $dynamic,
            'render_type' => $renderType,
        );
    }
}
