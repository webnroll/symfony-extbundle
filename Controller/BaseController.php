<?php
namespace Webnroll\ExtBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Webnroll\ExtBundle\Component\DynamicAssets;

class BaseController extends Controller
{
    /**
     * @return DynamicAssets
     */
    protected function getDynamicAssets() {
        return $this->container->get('ext.dynamic.assets');
    }

    /**
     * @return \Webnroll\ExtBundle\Component\Service\ViewPack
     */
    public function getViewPack()
    {
        return $this->get('ext.viewpack');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getMasterRequest() {
        return $this->getViewPack()->getRequest();
    }

}