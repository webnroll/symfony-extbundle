<?php

namespace Webnroll\ExtBundle\Component\Service;

use Webnroll\ExtBundle\Component\Helper\JsModelHelper;
use Webnroll\ExtBundle\Component\Service\ViewPack;
use Symfony\Component\Templating\Helper\Helper;

class JSNamespace {

    public function js_namespace($nameSpace)
    {
        echo '<script type="text/javascript" src="@RealtyMainBundle/Resources/public/js/'. $nameSpace .'.js" ></script>';
    }

    public function define($nameSpace, $o)
    {
        $o = JsModelHelper::prepareModel($o);
        return '<script type="text/javascript">$.ns("' . $nameSpace . '", ' . json_encode($o) . ');</script>';
    }

    public function parameters($nameSpace, $o)
    {
        $o = JsModelHelper::prepareModel($o);
        return '<script type="text/javascript">$.nsParameters("' . $nameSpace . '", ' . json_encode($o) . ').init();</script>';
    }

    public function parametersRaw($nameSpace, $o)
    {
        return '<script type="text/javascript">$.nsParameters("' . $nameSpace . '", ' . json_encode($o) . ').init();</script>';
    }


    public function getName()
    {
        return 'jsnamespace';
    }
}