<?php

namespace Webnroll\ExtBundle\Component\Service;

use Symfony\Component\Security\Acl\Exception\Exception;

class ViewPack {
    /** @var array */
    private $data = array();

    public function __get($name) {
        return isset($this->data[$name]) ? $this->data[$name] : null;
    }

    public function __set($name, $value) {
        $this->data[$name] = $value;
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function __unset($name)
    {
        unset($this->data[$name]);
    }

    public function __call($f, $p) {
        if (method_exists($this, $f.sizeof($p))) {
            return call_user_func_array(array($this, $f.sizeof($p)), $p);
        }
        else {
            if (preg_match('/(set|get)(.*)/', $f, $matches)) {
                $op = $matches[1];
                $var = strtolower($matches[2]);
                switch ($op) {
                    case 'set':
                        $this->data[$var] = $p[0];
                        return true;
                        break;
                    case 'get':
                        return isset($this->data[$var]) ? $this->data[$var] : null;
                        break;
                }
            }
            else {
                //  trying to return property
                return $this->__get($f);
            }
        }
    }

    public function getName()
    {
        return 'viewpack';
    }
}