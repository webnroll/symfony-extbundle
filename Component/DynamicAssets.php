<?php
namespace Webnroll\ExtBundle\Component;

use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
use Assetic\Asset\GlobAsset;

class DynamicAssets {
    protected $css = array();
    protected $cssInline = array();

    protected $js = array();
    protected $jsInline = array();

    protected $cssOrder = array();
    protected $jsOrder = array();

    public function __construct() {
    }

    public function addCss($url) {
        if (!in_array($url, $this->css)) {
            $this->cssOrder[] = array( 'type' => 'url', 'idx' => count($this->css));
            $this->css[] = $url;
        }

        return $this;
    }

    public function addCssInline($code) {
        $this->cssOrder[] = array( 'type' => 'inline', 'idx' => count($this->cssInline));
        $this->cssInline[] = $code;
        return $this;
    }

    public function addJs($url) {
        if (!in_array($url, $this->js)) {
            $this->jsOrder[] = array( 'type' => 'url', 'idx' => count($this->js));
            $this->js[] = $url;
        }

        return $this;
    }

    public function addJsInline($code) {
        $this->jsOrder[] = array( 'type' => 'inline', 'idx' => count($this->jsInline));
        $this->jsInline[] = $code;
        return $this;
    }

    public function getCss() {
        return $this->css;
    }

    public function getCssInline() {
        return $this->cssInline;
    }

    public function getJs() {
        return $this->js;
    }

    public function getJsInline() {
        return $this->jsInline;
    }

    public function getJsHtml() {
        $html = '';
        for ($i=0;$i<count($this->jsOrder);$i++) {
            $order = $this->jsOrder[$i];
            switch ($order['type']) {
                case 'url':
                    $html .= "<script type=\"text/javascript\" src=\"{$this->js[$order['idx']]}\"></script>\n";
                    break;
                case 'inline':
                    $html .= "<script type=\"text/javascript\">{$this->jsInline[$order['idx']]}</script>\n";
                    break;
            }
        }

        return $html;
    }

    public function getCssHtml() {
        $html = '';
        for ($i=0;$i<count($this->cssOrder);$i++) {
            $order = $this->cssOrder[$i];
            switch ($order['type']) {
                case 'url':
                    $html .= "<link rel=\"stylesheet\" href=\"{$this->css[$order['idx']]}\"/>\n";
                    break;
                case 'inline':
                    $html .= "<style type=\"text/css\">{$this->cssInline[$order['idx']]}</style>\n";
                    break;
            }
        }

        return $html;
    }
}