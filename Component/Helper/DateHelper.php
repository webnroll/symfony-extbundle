<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 16.10.13
 * Time: 11:52
 * To change this template use File | Settings | File Templates.
 */

namespace Webnroll\ExtBundle\Component\Helper;


class DateHelper
{
    private static $MonthEncoder = array(
        '_' => array("january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"),
        'i' => array("январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"),
        'r' => array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"),
        't' => array("январе", "феврале", "марте", "апреле", "мае", "июне", "июле", "августе", "сентябре", "октябре", "ноябре", "декабре"),
    );

    private static $MonthEncoderShort = array(
        "jan" => "янв",
        "feb" => "фев",
        "mar" => "мар",
        "apr" => "апр",
        "may" => "мая",
        "jun" => "июн",
        "jul" => "июл",
        "aug" => "авг",
        "sep" => "сен",
        "oct" => "окт",
        "nov" => "ноя",
        "dec" => "дек",
    );

    /**
     * @param string $s
     * @param string $pad
     * @return string
     */
    public static function encodeMonth($s, $pad = 'i')
    {
        $s = mb_strtolower($s);

        for ($i = 0; $i < count(self::$MonthEncoder[$pad]); $i++) {
            $monthSrc = self::$MonthEncoder['_'][$i];
            $monthRepl = self::$MonthEncoder[$pad][$i];
            $s = str_replace($monthSrc, $monthRepl, $s);
        }

        return $s;
    }

    /**
     * @param string $s
     * @return string
     */
    public static function encodeMonthShort($s)
    {
        $s = mb_strtolower($s);

        if (array_key_exists($s, self::$MonthEncoderShort)) {
            return self::$MonthEncoderShort[$s];
        }

        return $s;
    }

    public static function getMonthName($n) {
        return self::$MonthEncoder['i'][$n-1];
    }

    public static function isWeekend($date) {
        if (is_object($date)) {
            $n = $date->format('N');
        }
        else {
            $n = date('N', $date);
        }
        return in_array($n, array(6, 7));
    }

    public static function getDayCount($m) {
        $days = array(31,28,31,30,31,30,31,31,30,31,30,31);
        return $days[$m-1];
    }
}