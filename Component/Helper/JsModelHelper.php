<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 27.07.14
 * Time: 13:28
 */

namespace Webnroll\ExtBundle\Component\Helper;


use Realty\MainBundle\Model\BaseModel;
use Symfony\Component\Validator\Constraints\DateTime;

class JsModelHelper {

    public static function prepareModel($jsModel, $shortFormat = "d.m.Y", $longFormat = "d.m.Y H:i:s"){
        if ($jsModel == null || is_string($jsModel))
        {
            return $jsModel;
        }
        if (is_array($jsModel)){
            foreach($jsModel as &$valItem){
                $valItem = JsModelHelper::prepareModel($valItem, $shortFormat, $longFormat);
            }
            return $jsModel;
        }
        if (is_object($jsModel)){
            $reflect = new \ReflectionClass($jsModel);
            $props  = $reflect->getProperties();
            foreach ($props as $prop) {
                $val = $prop->getValue($jsModel);
                if ($val instanceof \DateTime){
                    $prop->setValue($jsModel, self::formatDateTime($val, $shortFormat, $longFormat));
                }
                elseif ($val instanceof BaseModel){
                    JsModelHelper::prepareModel($val, $shortFormat, $longFormat);
                }
                elseif (is_array($val)){
                    foreach($val as &$valItem){
                        $valItem = JsModelHelper::prepareModel($valItem, $shortFormat, $longFormat);
                    }
                }
            }
        }
        return $jsModel;
    }

    public static function prepareArray($model, $shortFormat = "d.m.Y", $longFormat = "d.m.Y H:i:s"){
        if ($model == null)
        {
            return $model;
        }
        if (is_array($model)){
            foreach($model as &$valItem){
                $valItem = JsModelHelper::prepareArray($valItem);
            }
            return $model;
        }
        if ($model instanceof \DateTime){
            $model = self::formatDateTime($model, $shortFormat, $longFormat);
        }
        return $model;
    }

    public static function formatDateTime(\DateTime $val, $shortFormat, $longFormat)
    {
        $woTime = \DateTime::createFromFormat('d.m.Y', ($val->format('d.m.Y')));
        if (!$woTime) {
            return '';
        }
        
        $woTime->setTime(0, 0, 0);
        $dif = abs($val->getTimestamp() - $woTime->getTimestamp());
        if ($dif > 0){
            return $val->format($longFormat);
        }
        else{
            return $val->format($shortFormat);
        }
    }
} 