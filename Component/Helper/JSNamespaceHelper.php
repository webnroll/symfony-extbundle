<?php

namespace Webnroll\ExtBundle\Component\Helper;

use Webnroll\ExtBundle\Component\Service\JSNamespace;
use Webnroll\ExtBundle\Component\Service\ViewPack;
use Symfony\Component\Templating\Helper\Helper;

class JSNamespaceHelper extends Helper {

    private $jsNamespace;

    public function __construct(JSNamespace $jsNamespace) {
        $this->jsNamespace = $jsNamespace;
    }

    public function js_namespace($nameSpace)
    {
        $this->jsNamespace->js_namespace($nameSpace);
    }


    public function parameters($nameSpace, $o)
    {
        return $this->jsNamespace->parameters($nameSpace, $o);
    }


    public function getName()
    {
        return 'jsnamespace';
    }
}