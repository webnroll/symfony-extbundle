<?php

namespace Webnroll\ExtBundle\Component\Helper;

use Webnroll\ExtBundle\Component\Service\ViewPack;
use Symfony\Component\Templating\Helper\Helper;

class ViewPackHelper extends Helper {
    private $viewPack;

    public function __construct(ViewPack $viewPack) {
        $this->viewPack = $viewPack;
    }

    public function __get($name) {
        return $this->viewPack->__get($name);
    }

    public function __set($name, $value) {
        $this->viewPack->__set($name, $value);
    }

    public function __isset($name)
    {
        return $this->viewPack->__isset($name);
    }

    public function __unset($name)
    {
        $this->viewPack->__unset($name);
    }

    public function __call($f, $p) {
	return $this->viewPack->__call($f, $p);
    }

    public function getName()
    {
        return 'viewpack';
    }
}