<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 16.10.13
 * Time: 11:52
 * To change this template use File | Settings | File Templates.
 */

namespace Webnroll\ExtBundle\Component\Helper;


class StringHelper
{

    public static function ellipse($input, $length, $ellipses = true, $strip_html = true)
    {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }
        $input = trim($input);

        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');

        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }

        return $trimmed_text;
    }

    public static function isSetConcat($var, $str)
    {
        if ($var) {
            return $str . $var;
        }
        return null;
    }

    public static function startsWith($haystack, $needle)
    {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }

    public static function endsWith($haystack, $needle)
    {
        return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
    }

    public static function mb_strcasecmp($str1, $str2, $encoding = null)
    {
        if (null === $encoding) {
            $encoding = mb_internal_encoding();
        }
        return strcmp(mb_strtoupper($str1, $encoding), mb_strtoupper($str2, $encoding));
    }

    public static function mb_str_replace($needle, $replacement, $haystack)
    {
        return implode($replacement, mb_split($needle, $haystack));
    }

    public static function mb_transliterate($string)
    {
        $table = array(
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D',
            'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
            'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
            'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH',
            'Ш' => 'SH', 'Щ' => 'SCH', 'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
            'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA',

            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd',
            'е' => 'e', 'ё' => 'yo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i',
            'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
            'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sch', 'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
        );

        $output = str_replace(
            array_keys($table),
            array_values($table), $string
        );

        // также те символы что неизвестны
        $output = preg_replace('/[^-a-z0-9._\[\]\'"]/i', ' ', $output);
        $output = preg_replace('/ +/', '-', $output);

        return $output;
    }

    public static function numberToString($n)
    {
        return str_replace('.', ',', (string)$n);
    }

    public static function intervalSplit($s, $separator = '-')
    {
        $l = explode(',', $s);
        $result = array();
        for ($i = 0; $i < count($l); $i++) {
            $n = trim($l[$i]);
            $l2 = explode($separator, $n);
            if (count($l2) >= 2 && intval($l2[1]) > intval($l2[0])) {
                for ($t = intval($l2[0]); $t <= intval($l2[1]); $t++) {
                    $result[] = $t;
                }
            } else {
                $result[] = intval($l2[0]);
            }
        }

        return $result;
    }

    /**
     * Возвращает сумму прописью
     * @author runcore
     * @uses morph(...)
     */
    public static function num2Str($num, $processFraction = false)
    {
        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        );
        $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
        $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $unit = array( // Units
            array('', '', '', 1),
            array('', '', '', 0),
            array('тысяча', 'тысячи', 'тысяч', 1),
            array('миллион', 'миллиона', 'миллионов', 0),
            array('миллиард', 'милиарда', 'миллиардов', 0),
        );

        //
        $e = explode('.', (string)$num);
        if (count($e) > 1) {
            list($int, $fract) = $e;
        } else {
            $int = $num;
        }

        $int = sprintf("%012d", $int);
        $out = array();

        if (!$processFraction) {
            if (intval($int) > 0) {
                foreach (str_split($int, 3) as $uk => $v) { // by 3 symbols
                    if (!intval($v)) continue;
                    $uk = sizeof($unit) - $uk - 1; // unit key
                    $gender = $unit[$uk][3];
                    list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                    // mega-logic
                    $out[] = $hundred[$i1]; # 1xx-9xx
                    if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
                    else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                    // units without rub & kop
                    if ($uk > 1) $out[] = self::morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
                } //foreach
            } else {
                $out[] = $nul;
            }
        } else {
            if (intval($fract) > 0) {
                $split = str_split($fract, 1);
                if (count($split) >= 2) {
                    list($i1, $i2) = array_map('intval', str_split($fract, 1));
                } else {
                    $i1 = intval($fract);
                    $i2 = 0;
                }

                if ($fract >= 10 && $fract < 20) {
                    $out[] = $a20[$fract - 10];
                } elseif ($fract >= 20) {
                    $out[] = $tens[$i1] . ' ' . $ten[1][$i2];
                } else {
                    if ($i1>0) {
                        $out[] = $ten[1][$i1];
                    }
                    else {
                        $out[] = $ten[1][$i2];
                    }
                }
            } else {
                $out[] = $nul;
            }
        }

        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    public static function getFract($num, $precision = null)
    {
        if ($precision) {
            $num = number_format($num, $precision);
        }

        $fract = 0;
        if (strpos((string)$num, '.')!==false) {
            list(, $fract) = explode('.', (string)$num);
        }

        return $fract;
    }

    public static function getFractLength($num, $precision = null)
    {
        if ($precision) {
            $num = number_format($num, $precision);
        }
        list(, $fract) = explode('.', (string)$num);
        return strlen($fract);
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    public static function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5) return $f2;
        if ($n == 1) return $f1;
        return $f5;
    }

    /**
     * Дата прописью
     * @param $date_input
     * Формат dd.MM.YYYY
     * @return string
     */
    public static function fullDateString($date_input)
    {
        $date = explode(".", $date_input);
        $day = (int)$date[0];
        $mon = (int)$date[1];
        $year = (int)$date[2];
        $mons = array("", "января", "февраля", "марта", "апреля", "мая", "июня",
            "июля", "августа", "сентября", "октября", "ноября", "декабря");
        $a1 = array("", "первое", "второе", "третье", "четвёртое", "пятое", "шестое",
            "седьмое", "восьмое", "девятое", "десятое", "одиннадцатое", "двенадцатое",
            "тринадцатое", "четырнадцатое", "пятнадцатое", "шестнадцатое", "семнадцатое",
            "восемнадцатое", "девятнадцатое", "двадцатое", "тридцатое");
        $a2 = array("", "первого", "второго", "третьего", "четвёртого", "пятого",
            "шестого", "седьмого", "восьмого", "девятого", "десятого", "одиннадцатого",
            "двенадцатого", "тринадцатого", "четырнадцатого", "пятнадцатого",
            "шестнадцатого", "семнадцатого", "восемнадцатого", "девятнадцатого",
            "двадцатого", "тридцатого", "сорокогого", "пятидесятого", "шестидесятого",
            "семидесятого", "восьмидесятого", "девяностого", "сотого");
        $a3 = array("", "одна", "две", "три", "четыре", "пять", "шесть", "семь",
            "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать",
            "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать",
            "девятнадцать", "двадцать", "тридцать", "сорок", "пятидесят", "шестьдесят",
            "семьдесят", "восемьдесят", "девяносто", "сто");
        $a4 = array("", "сто", "двести", "тристо", "четыресто", "пятьсот", "шестьсот",
            "семьсот", "восемьсот", "девятьсот");
        $a5 = array("", "сотого", "двух", "трёх", "четырёх", "пяти", "шести", "семи",
            "восьми", "девяти");
        $a6 = array("", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят",
            "семьдесят", "восемдесят", "девяносто");
        $thousand = array("", "тысяча", "тысячи", "тысяч", "тысячного");

        if ($day <= count($a1) - 2) {
            $daystr = $a1[$day];
        } elseif ($day > 20 & $day < 30) {
            $ended = $day - 20;
            $daystr = $a6[1] . " " . $a1[$ended];
        } elseif ($day == 30) {
            $daystr = $a1[21];
        } elseif ($day > 30) {
            $daystr = $a6[2] . " " . $a1[1];
        }
        $monstr = $mons[$mon];
        $t = floor($year / 1000);
        if ($t == 1) {
            if (($year % 1000) == 0) $ystr = $a3[1] . $thousand[4];
            else $ystr = $a3[$t] . " " . $thousand[1];
        } elseif ($t > 1 & $t < 5) {
            if (($year % 1000) == 0) $ystr = $a5[$t] . $thousand[4];
            else $ystr = $a3[$t] . " " . $thousand[2];
        } elseif ($t >= 5) {
            if (($year % 1000) == 0) $ystr = $a5[$t] . $thousand[4];
            else $ystr = $a3[$t] . " " . $thousand[3];
        }
        $r = $year - ($t * 1000);
        $h = floor($r / 100);
        $r = $r - ($h * 100);
        $d = floor($r / 10);
        $r = $r - ($d * 10);
        if ($h <> 0 & $d == 0 & $r == 0) {
            if ($h == 1) $ystr100 = $a5[$h];
            else $ystr100 = $a5[$h] . $a5[1];
        } else $ystr100 = $a4[$h];
        if ($d <> 0 & $r == 0) {
            if ($d == 1) {
                $ystr10 = $a2[10];
            } else {
                $ystr10 = $a2[$d + 18];
            }
        } else {
            $ystr10 = $a6[$d - 1];
        }
        if ($d == 1 & $r > 0 & $r <= 9) {
            $ystr1 = $a2[($d * 10) + $r];
        } else {
            $ystr1 = $a2[$r];
        }

        $l = array($daystr, $monstr);
        if ($ystr) {
            $l[] = $ystr;
        }
        if ($ystr100) {
            $l[] = $ystr100;
        }
        if ($ystr10) {
            $l[] = $ystr10;
        }
        if ($ystr1) {
            $l[] = $ystr1;
        }

        return implode(' ', $l);
    }

    public static function wrap($str, $start, $end)
    {
        return $start . $str . $end;
    }

    public static function capitalize($string)
    {
        return mb_strtoupper(mb_substr($string, 0, 1)) .
        mb_strtolower(mb_substr($string, 1, mb_strlen($string)));
    }

}