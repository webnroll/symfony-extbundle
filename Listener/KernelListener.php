<?php
namespace Webnroll\ExtBundle\Listener;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class KernelListener
{
    protected $container;
    protected $spaceless;

    public function __construct(ContainerInterface $container, $spaceless)
    {
        $this->container = $container;
        $this->spaceless = $spaceless;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if (HttpKernelInterface::MASTER_REQUEST === $event->getRequestType()) {
            $this->container->get('ext.viewpack')->setRequest($request);
        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response = $event->getResponse();
        $request = $event->getRequest();

        if ($event->getRequestType() == HttpKernelInterface::MASTER_REQUEST
            &&
            $request->getRequestFormat() == 'html'
            && !($response instanceof BinaryFileResponse)
        ) {
            //	set P3P common headers
            $response->headers->set('P3P', 'CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

            $content = $response->getContent();
            $content = preg_replace(
                '/<dynamic_assets_js\/>/',
                $this->container->get('ext.dynamic.assets')->getJsHtml(),
                $content
            );
            $content = preg_replace(
                '/<dynamic_assets_css\/>/',
                $this->container->get('ext.dynamic.assets')->getCssHtml(),
                $content
            );

            if ($this->spaceless) {
                $content = preg_replace('/>\s+</', '><', $content);
            }

            $response->setContent($content);
        }
    }
}